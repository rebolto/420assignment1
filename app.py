from flask import Flask, request, render_template
import random
app = Flask(__name__)

@app.route("/")
def home():
    return render_template('exercises.html')

@app.route("/exercise1")
def exercise1():
    return render_template('exercise1.html')

@app.route("/exercise2")
def exercise2():
    return render_template('exercise2.html')

@app.route("/exercise3")
def exercise3():
    return render_template('exercise3.html')

@app.route("/exercise4")
def exercise4():
    return render_template('exercise4.html')

@app.route("/exercise5")
def exercise5():
    return render_template('exercise5.html')

@app.route('/exercise1', methods=['POST'])
def exercise1_post():
    dividend = int(request.form['dividend'])
    divisor = int(request.form['divisor'])
    message=""
    if dividend%2==0:
        message+=f'{dividend}is even\n<br>'
        if dividend%4==0:
            message+=f'{dividend} is multiple of 4x`<br>'
    else:
        message+=f'{dividend} is odd<br>'
    if dividend%divisor==0:
        message+=f'{dividend} is a multiple of {divisor}<br>'
    return f'<p>{message}</p>'

@app.route('/exercise2', methods=['POST'])
def exercise2_post():
    name = request.form['name']
    grade = request.form['grade']
    try:
       int(grade)
       if int(grade)<0 or int(grade)>100:
           raise Exception("Number has to be greater than 0 and lower than 100")
    except:
        return'''
                            <h1>Exercise 2</h1>
                            <p>In this exercise perform the following:</p>
                            <ul>
                                <li>Map the value to a letter grade using the following scheme &gt;=95 A, 80&lt;=B&lt;95, 79&lt;=C&lt;=70, 69&lt;=D&lt;=60, &lt;=59 F</li>
                                <li>Display the result to the user</li>
                                <li>If the value is outside the range between 0 and 100 indicate to the user to provide the information again</li>
                            </ul>
                            <form method="POST">
                                <input name="name" placeholder="name">
                                <input name="grade" placeholder="grade">
                                <input type="submit">
                            </form>
            '''
    if int(grade)<0 or int(grade) > 100:
        return f'<p>{name} wrong score: {int(grade)}'
    elif int(grade)>=95:
        return f'<p>{name} your score is {int(grade)}--> you get A</p>'
    elif int(grade)>=80 and int(grade)<95:
        return f'<p>{name} your score is {int(grade)}--> you get B</p>'
    elif int(grade) <=79 and int(grade) >=70:
        return f'<p>{name} your score is {int(grade)}--> you get C</p>'
    elif int(grade)>=60  and int(grade)<=69:
        return f'<p>{name} your score is {int(grade)}--> you get D</p>'
    else:
        return f'<p>{name} your score is {int(grade)}--> you get F</p>'
    


    

@app.route('/exercise3', methods=['POST'])
def exercise3_post():
    value = request.form['value']
    try:
        int(value)
        if int(value)<0:
            raise Exception('Negative numbers cant be square rooted')
    except:
        return '''
            <h1>Exercise 3</h1>
        <p>In this exercise perform the following:</p>
        <ul>
            <li>Compute the square root of the value passed by the user</li>
            <li>Display the result to the user</li>
            <li>If the value is invalid indicate to the user to provide the information again</li>
        </ul>
        <form method="POST">
            <input name="value" placeholder="value">
            <input type="submit">
        </form>
            '''
    return f'<p>Square root of {int(value)} is {int(value)**0.5}'

@app.route('/exercise4', methods=['POST'])
def exercise4_post():
    number = request.form['number']
    try:
        int(number)
        if int(number)<0 or int(number)>100:
            raise Exception("Number cannot be lower than 0 or greater than 100")
    except:
        return'''
            <h1>Exercise 4</h1>
        <p>In this exercise perform the following:</p>
        <ul>
            <li>Generate a random number between 1 and 100 </li>
            <li>Compare the generated number with the number entered</li>
            <li>If the numbers don't match generate a new random number</li>
            <li>Stop when the numbers are equal. Keep track of how many tries it took to match the numbers</li>
            <li>Display this information to the user</li>
        </ul>
        <form method="POST">
            <input name="number" placeholder="number between 0-100">
            <input type="submit">
        </form>
                '''
    random_num=random.randint(1,100)
    tries=0
    while int(number)!=random_num:
        tries+=1
        random_num=random.randint(1,100)
    return f'<p> It took {tries} tries to match {int(number)} to {random_num}</p>'

@app.route('/exercise5', methods=['POST'])
def exercise5_post():
    number = request.form['number']
    try:
        int(number)
        if int(number)<1:
            raise Exception("Number cannot be lower than 1")
    except:
        return '''
            <h1>Exercise 5</h1>
        <p>In this exercise perform the following:</p>
        <ul>
            <li>Generate a display the Fibonacci Sequence up to the N-th number</li>
            <li>For information about the Fibonacci Sequence, see here: <a href="https://www.mathsisfun.com/numbers/fibonacci-sequence.html">Fibonacci</a></li>
        </ul>
        <form method="POST">
            <input name="number" placeholder="number > 1">
            <input type="submit">
        </form>
               '''
    sum=1
    num1=0
    num2=1
    message=f"Given {int(number)} Fibonachi------> 0, 1, "
    while sum<int(number):
        message+=f'{sum}, '
        num1 = num2
        num2 = sum
        sum=num1 + num2
  
    return f'<p>{message}</p>'